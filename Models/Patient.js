'use strict'

const Sequelize = require('sequelize');
const sequelize = require('../DataBase/DataBase');

const patient = sequelize.define('patients',{
    id_de_caso :{type: Sequelize.INTEGER, primaryKey: true},
    fecha_de_notificaci_n: Sequelize.DATE,
    c_digo_divipola:Sequelize.STRING,
    ciudad_de_ubicaci_n:Sequelize.STRING,
    departamento:Sequelize.STRING,
    atenci_n:Sequelize.STRING,
    edad:Sequelize.SMALLINT,
    sexo:Sequelize.CHAR,
    tipo:Sequelize.STRING,
    estado:Sequelize.STRING,
    pa_s_de_procedencia:Sequelize.STRING,
    fis: Sequelize.DATE,
    fecha_diagnostico:Sequelize.DATE,
    fecha_recuperado:Sequelize.DATE,
    fecha_reporte_web:Sequelize.DATE,
    tipo_recuperaci_n:Sequelize.STRING,
    codigo_departamento:Sequelize.STRING,
    codigo_pais:Sequelize.STRING,
    pertenencia_etnica:Sequelize.STRING,
    ubicaci_n_recuperado:Sequelize.STRING
});

module.exports = patient;