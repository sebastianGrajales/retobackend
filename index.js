'use strict'
const app = require('./app');
const config = require('./conf');
const sequelize = require('./DataBase/DataBase');

sequelize.sync()
.then(()=>{
    const executor = require('./Controllers/ControllerSQL');
})
.then(()=>{
    app.listen(config.port,()=>{
        console.log('api running');
    });
});

