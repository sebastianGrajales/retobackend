'use strict'
const express = require('express');
const app = express();
const conf = require('./conf');
const bodyParser = require('body-parser');
const api = require('./Routes');
const multer = require('multer');
const upload = multer();

app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.use(upload.array()); 
app.use(express.static('public'));
app.use('/api', api);
app.set('port',conf.port);

module.exports= app;