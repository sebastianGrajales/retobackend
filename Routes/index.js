'use strict'

const express = require('express');
const api = express.Router();
const apiController = require('../Controllers/ControllerApi');

api.get('/patient/:gender',apiController.filterGender);
api.post('/patient',apiController.filterAge);
api.post('/seeFiltersData',apiController.GetByFilters);
module.exports= api