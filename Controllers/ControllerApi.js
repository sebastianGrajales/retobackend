'use strict'
const request = require('request');
const patient = require('../Models/Patient');

function filterGender(req,res){
    let gender = req.params.gender
    let patientList = [];
    if(gender.toUpperCase()!= 'F' && gender.toUpperCase()!= 'M'){
        res.status(400).send({message: 'You must enter f for feminine or m for masculine'});
    }
    else{
        getData(function(err, body){
            if(err){
                res.status(500).send({message: `Failed to server: ${err}`});
            }
            for (let index in body){
                if(body[index].sexo == gender.toUpperCase()){
                    patientList.push(body[index]);
                }
            }
            res.status(200).send({pascients:patientList});
        });
    }
}

function filterAge(req,res){
    let ageMin = req.body.ageMin
    let ageMax = req.body.ageMax
    let listPascients = [];
    if(ageMin < 0 && ageMax < 0){
        return res.status(400).send({message: 'you must enter a valid age'});
    }
    else if((ageMin!=0 || ageMax!=20)&&(ageMin!=20 || ageMax!=40)&&(ageMin!=40)){
        return res.status(400).send({message: 'error in the filters'});
    }
    else{
        getData(function(err, body){
            if(err){
                res.status(500).send({message: `Failed to server: ${err}`});
            }
            for (let index in body){
                if(ageMin < 40 && body[index].edad >= ageMin && body[index].edad < ageMax) {
                    listPascients.push(body[index]);
                }
                else if(ageMin==40 && body[index].edad >= ageMin){
                    listPascients.push(body[index]);
                }
            }
            res.status(200).send({pascients:listPascients});
        });
    }
}

async function getData(callback){
    request('https://www.datos.gov.co/resource/gt2j-8ykr.json',function (error,res, body){
        if(error) callback(error,null);
        callback(null,JSON.parse(body));
    }) 
}

async function GetByFilters (req,res){
    let city = req.body.ciudad_de_ubicaci_n;
    let gender = req.body.sexo;
    let type = req.body.tipo;
    let filters = {}
    if(city!=undefined){
        filters.ciudad_de_ubicaci_n = city;
    }
    if(gender!=undefined){
        filters.sexo = gender.toUpperCase();
    }
    if(type!=undefined){
        filters.tipo = type;
    }
    patient.findAll({
        where : filters
    }).then(patients=>{
        let listPatients = [];
        for(let element in patients){
            listPatients.push({
                id_de_caso : patients[element].id_de_caso
            });
        }
        res.status(200).send(listPatients);
    })
    .catch(err=>{
        res.status(500).send({message:err});
    })
}

module.exports={
    filterGender,
    filterAge,
    GetByFilters
}