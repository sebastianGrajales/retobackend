'use strict'
const request = require('request');
const Events = require('events');
const  eventIssuer = Events.EventEmitter; 
const event = new eventIssuer(); 
const patient = require('../Models/Patient')


async function getInformation(callback){
    request('https://www.datos.gov.co/resource/gt2j-8ykr.json',function (error,res, body){
        if(error) callback(error,null);
        callback(null,JSON.parse(body));
    })
}

function fillDataBase(){
    console.log('Creating BD');
    getInformation((err,body)=>{
        for(let index in body){
            patient.sync();
            patient.findOrCreate({
                where : {id_de_caso: body[index].id_de_caso},
                defaults: {
                    id_de_caso : body[index].id_de_caso,
                    fecha_de_notificaci_n: body[index].fecha_de_notificaci_n,
                    c_digo_divipola: body[index].c_digo_divipola,
                    ciudad_de_ubicaci_n: body[index].ciudad_de_ubicaci_n,
                    departamento: body[index].departamento,
                    atenci_n: body[index].atenci_n,
                    edad: body[index].edad,
                    sexo: body[index].sexo,
                    tipo: body[index].tipo,
                    estado: body[index].estado,
                    pa_s_de_procedencia: body[index].pa_s_de_procedencia,
                    fis: body[index].fis,
                    fecha_diagnostico: body[index].fecha_diagnostico,
                    fecha_recuperado: body[index].fecha_recuperado,
                    fecha_reporte_web: body[index].fecha_reporte_web,
                    tipo_recuperaci_n: body[index].tipo_recuperaci_n,
                    codigo_departamento: body[index].codigo_departamento,
                    codigo_pais: body[index].codigo_pais,
                    pertenencia_etnica: body[index].pertenencia_etnica,
                    ubicaci_n_recuperado: body[index].ubicaci_n_recuperado
                }
            }).then(()=>{
                console.log(`Dato ${index} creado`);
            })
            .catch((err)=>{
                console.log(err);
            });
        }
    });
}

function generateBD(){
    setImmediate(fillDataBase);
    setInterval(fillDataBase,1000*60*10); 
}
event.on('fillDataBase', generateBD);
event.emit('fillDataBase'); 


module.exports = event;