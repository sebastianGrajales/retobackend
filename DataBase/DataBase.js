'use strict'

const Sequelize = require('sequelize');
const config = require('../conf');

const sequelize = new Sequelize('postgres','postgres','12345',{
  host: config.host,
  dialect: 'postgres',
  loggin: false
});
sequelize.authenticate()
  .then(() => {
    console.log('BD Connected')
  })
  .catch(err => {
    console.log(`connection error: ${err}`)
  });
  
module.exports= sequelize;